/**
 * Creates .js file containing icons.
 */

const fs = require('fs');
const fse = require('fs-extra');

const DIR = './icons-optimized';
const DIST_DIR = './dist/js';

async function build() {
    const files = fs.readdirSync(DIR, { withFileTypes: true });

    let result = '';

    for (const file of files) {
        if (file.isFile()) {
            result += `'${file.name.replace('.svg', '')}': '`;
            const fileData = fs.readFileSync(`${DIR}/${file.name}`, { encoding: 'utf8' });
            result += fileData.replace(/(?:\r\n|\r|\n)/g, '');
            result += `',\n`;
        }
    }

    const template = fs.readFileSync('./src/dist.template.js', { encoding: 'utf8' });
    const ukExtended = template.replace(/REPLACE_ICONS/gi, result);

    await fse.outputFile(`${DIST_DIR}/uk-extended.js`, ukExtended);
}

build();
